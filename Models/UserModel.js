const mongoose = require('mongoose');
var Schema = mongoose.Schema
// mongoose.Promise = global.Promise;

var UserModel = new mongoose.Schema( {
 email : String,
 password : String
},{ collection: 'users'});

var User = mongoose.model('User',UserModel);

module.exports = User;