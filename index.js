var expressServer = require('express');
var dataparser = require('body-parser');
var User = require('./Models/UserModel.js');
var dbName = require('mongoose');
var app = expressServer();

app.use(dataparser.json());
const port = 3001;

app.get('/welcome',(req,res) => {
  res.send('Welcome to TaskManager app')
});

dbName.connect('mongodb://localhost/taskmanager',function(err){
  if (err) {
    console.log('error ' +err);
  } else{
     console.log('connection established ');
}
});

app.post('/register',(request,response) => {
      createUser(request,response);
});

app.post('/login',(request,response) => {
    loginUser(request,response);
});

app.listen(port, () => {
console.log("listening to client on port" + port);
});

function createUser(request,response) {
 response.header("Access-Control-Allow-Origin", "*");
const requestBody = request.body;
console.log("request received" + requestBody.email);

var user = new User( {
	email:requestBody.email,
	password:requestBody.password
});

user.save(function(err,result) {
if (err) {
	response.send(err);
}
else {
	console.log("User created successfully");
	console.log("result"+result);
	response.send(result);	
}
});

}

function loginUser(request,response) {
 response.header("Access-Control-Allow-Origin", "*");
 const requestBody = request.body;
 console.log("request received" + requestBody.email);
const email = requestBody.email;
const password = requestBody.password;
// try {
// User.findOne({
// 	'email':email,
// 	'password':password
// },function (err,user) {
// 	if (err) {
// 		console.log("Error"+err);
// 	}
// 	else {
// 		mongoose.connection.close();
//       console.log(users);
// 	}
// })
// }
// catch (error) {
	// console.log("Error handled here");
	// console.log("Error description:"+error.message);
// }

User.findOne({ 'email': email,'password' :password}, 'email', function (err, person) {
  if (err) response.send(err);
  console.log('%s',person.email);
  response.send("logged in successfully"); // Space Ghost is a talk show host.
})
}

