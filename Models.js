var mongoose = require("mongoose");
var User = require("./Models/UserModel"); 

mongoose.connect("mongodb://localhost/test");  
 
var db = mongoose.connection;
 
db.on("error", console.error.bind(console, "Connection error:"));
db.once("open", function(callback){
     console.log("Connection Succeeded."); 
 });
 
var User = mongoose.model("User", User.UserModel); 
 
module.exports.User = User;